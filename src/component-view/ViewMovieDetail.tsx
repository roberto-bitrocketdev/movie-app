import { FC, useState, useEffect } from "react";
import { TMovie, TMovies } from "../App";
import { Link, useParams } from "react-router-dom";

export interface Genre {
  id: number;
  name: string;
}
export interface ProductionCompany {
  id: number;
  logo_path?: any;
  name: string;
  origin_country: string;
}
export interface ProductionCountry {
  iso_3166_1: string;
  name: string;
}
export interface SpokenLanguage {
  english_name: string;
  iso_639_1: string;
  name: string;
}
export interface TMovieDetail {
  adult: boolean;
  backdrop_path: string;
  belongs_to_collection?: any;
  budget: number;
  genres: Genre[];
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  production_companies: ProductionCompany[];
  production_countries: ProductionCountry[];
  release_date: string;
  revenue: number;
  runtime: number;
  spoken_languages: SpokenLanguage[];
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export const ViewMovieDetail: FC = () => {
  const [detailMovie, setDetailMovie] = useState<TMovieDetail>();
  const [similarMovie, setSimilarMovie] = useState<TMovie[]>();
  const { idMovie } = useParams<any>();
  const getMovieDetail = async () => {
    const response = await fetch(
      `https://api.themoviedb.org/3/movie/${idMovie}?api_key=e96ffa2380211d4e41d4f9a6318f7504&language=it-IT`
    );
    const movie: TMovieDetail = await response.json();
    setDetailMovie(movie);
  };
  const getSimilarMovie = async () => {
    const response = await fetch(
      `https://api.themoviedb.org/3/movie/${idMovie}/similar?api_key=e96ffa2380211d4e41d4f9a6318f7504&language=it-IT&page=1`
    );
    const similarMovieList: TMovies = await response.json();
    const movieList = similarMovieList.results;
    setSimilarMovie(movieList);
  };
  const renderSimilarMovie = (similarMovie: TMovie) => (
    <div key={similarMovie.id}>
      <Link
        style={{ textDecoration: "none", color: "black" }}
        to={`/movie/${similarMovie.id}`}
      >
        <div
          style={{
            border: "2px solid grey",
            background: "lightBlue",
            padding: "10px",
            borderRadius: "10px",
            color: "black",
            margin: "15px",
          }}
        >
          <img
            height="400px"
            width="250px"
            src={`https://image.tmdb.org/t/p/w300/${similarMovie.poster_path}`}
            alt={similarMovie.original_title}
          />
          <div>{similarMovie.original_title}</div>
        </div>
      </Link>
    </div>
  );
  useEffect(() => {
    getMovieDetail();
    getSimilarMovie();
  }, [similarMovie]);
  return (
    <>
      <Link to="/">
        <button>HOME</button>
      </Link>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            width: "fit-content",
            border: "2px solid grey",
            background: "lightBlue",
            padding: "10px",
            borderRadius: "10px",
            color: "black",
            margin: "15px",
          }}
        >
          <img
            src={`https://image.tmdb.org/t/p/w300/${detailMovie?.poster_path}`}
            alt={detailMovie?.original_title}
          />
        </div>
      </div>
      <div>{detailMovie?.overview}</div>
      <h3 style={{ display: "flex", justifyContent: "center" }}>
        Top Similar Movie
      </h3>
      <div style={{ display: "flex", overflowX: "scroll" }}>
        {similarMovie?.map(renderSimilarMovie)}
      </div>
    </>
  );
};
