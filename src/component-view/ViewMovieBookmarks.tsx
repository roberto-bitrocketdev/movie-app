import React, { FC } from "react";
import { Link } from "react-router-dom";
import { TMovie } from "../App";

interface Props {
  handleRemoveBookmarks: (movie: TMovie) => void;
  bookmarks: TMovie[];
}

export const ViewMovieBookmark: FC<Props> = ({
  bookmarks,
  handleRemoveBookmarks,
}) => {
  const renderBookmarks = (bookmark: TMovie) => (
    <div key={bookmark.id}>
      <div
        style={{
          border: "2px solid grey",
          background: "lightBlue",
          padding: "10px",
          borderRadius: "10px",
          color: "black",
          margin: "15px",
          width: "fit-content",
        }}
      >
        <Link style={{ textDecoration: "none" }} to={`/movie/${bookmark.id}`}>
          <img
            height="500px"
            width="350px"
            src={`https://image.tmdb.org/t/p/w300/${bookmark.poster_path}`}
            alt={bookmark.original_title}
          />
        </Link>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>{bookmark.original_title}</div>
          <span
            style={{ cursor: "pointer" }}
            onClick={() => handleRemoveBookmarks(bookmark)}
          >
            Remove
          </span>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Link to="/">
        <button>HOME</button>
      </Link>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        {bookmarks.map(renderBookmarks)}
      </div>
    </div>
  );
};
