import { ChangeEvent, FC, useState, useEffect } from "react";
import { TMovie, TMovies } from "../App";
import { Link } from "react-router-dom";

interface Props {
  onAddBookmark: (movie: TMovie) => void;
  handleRemoveBookmarks: (movie: TMovie) => void;
  bookmarks: TMovie[];
}

export const ViewHome: FC<Props> = ({
  onAddBookmark,
  handleRemoveBookmarks,
  bookmarks,
}) => {
  const [movies, setMovies] = useState<TMovie[]>([]);
  const [numberPage, setNumberPage] = useState(1);
  const [input, setInput] = useState<string>("");
  const [moviesFilter, setMoviesFilter] = useState<TMovie[]>([]);

  const getMovies = async () => {
    const response = await fetch(
      `https://api.themoviedb.org/3/movie/popular?api_key=c8214bf65a9ffd73757b863c4bc8bec2&language=it-IT&page=${numberPage}`
    );
    const res: TMovies = await response.json();
    const movieList = res.results;
    setMovies([...movies, ...movieList]);
  };

  const handleChange = async (value: string) => {
    setInput(value);
    const response = await fetch(
      `https://api.themoviedb.org/3/search/movie?api_key=e96ffa2380211d4e41d4f9a6318f7504&language=it-IT&query=${value}&page=1&include_adult=true`
    );
    const res: TMovies = await response.json();
    const movieListFilter = res.results;
    setMoviesFilter(movieListFilter);
  };

  const renderMovie = (movie: TMovie) => {
    const isOnBookmarks = bookmarks.some(
      (bookmark) => bookmark.id === movie.id
    );

    const bookmarksToogle = () => {
      if (isOnBookmarks) return "★";
      return "☆";
    };

    const onClickBookmark = () => {
      isOnBookmarks ? handleRemoveBookmarks(movie) : onAddBookmark(movie);
    };

    return (
      <div key={movie.id}>
        <div
          style={{
            border: "2px solid grey",
            background: "lightBlue",
            padding: "10px",
            borderRadius: "10px",
            color: "black",
            margin: "15px",
          }}
        >
          <Link style={{ textDecoration: "none" }} to={`/movie/${movie.id}`}>
            <img
              height="500px"
              width="350px"
              src={`https://image.tmdb.org/t/p/w300/${movie.poster_path}`}
              alt={movie.original_title}
            />
          </Link>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>{movie.original_title}</div>
            <span style={{ cursor: "pointer" }} onClick={onClickBookmark}>
              {bookmarksToogle()}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const onNextPage = () => {
    setNumberPage(numberPage + 1);
  };

  const handleTop = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };

  useEffect(() => {
    getMovies();
  }, [numberPage]);

  console.log(bookmarks);

  return (
    <div id="top">
      <Link
        to={{
          pathname: "/movie/bookmarks",
          state: bookmarks,
        }}
      >
        <button>BOOKMARKS</button>
      </Link>
      <button
        onClick={handleTop}
        style={{ position: "fixed", top: "80%", right: "0" }}
      >
        TOP
      </button>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          margin: "10px",
          alignItems: "center",
        }}
      >
        <input
          placeholder="Search"
          value={input}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            handleChange(e.target.value)
          }
        />
      </div>
      <div
        style={{
          display: "flex",
          overflowX: "scroll",
          flexWrap: "wrap",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        {!input ? movies.map(renderMovie) : moviesFilter?.map(renderMovie)}
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          margin: "50px 0px",
        }}
      >
        <button onClick={onNextPage}>Load More</button>
      </div>
    </div>
  );
};
