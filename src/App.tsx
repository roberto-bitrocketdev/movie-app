import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ViewHome } from "./component-view/ViewHome";
import { ViewMovieBookmark } from "./component-view/ViewMovieBookmarks";
import { ViewMovieDetail } from "./component-view/ViewMovieDetail";

export interface TMovies {
  page: number;
  results: TMovie[];
  total_pages: number;
  total_results: number;
}
export interface TMovie {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export const App = () => {
  const [bookmarks, setBookmarks] = useState<TMovie[]>([]);

  const onAddBookmark = (movie: TMovie) => {
    setBookmarks([...bookmarks, movie]);
    // localStorage.setItem(`${movie.id}`, JSON.stringify(movie));
  };

  const handleRemoveBookmarks = (movie: TMovie) => {
    const newBookmarks = bookmarks.filter(
      (bookmark) => bookmark.id !== movie.id
    );
    setBookmarks(newBookmarks);

    // localStorage.removeItem(`${movie.id}`);
  };

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <ViewHome
            bookmarks={bookmarks}
            onAddBookmark={onAddBookmark}
            handleRemoveBookmarks={handleRemoveBookmarks}
          />
        </Route>
        <Route exact path="/movie/bookmarks">
          <ViewMovieBookmark
            bookmarks={bookmarks}
            handleRemoveBookmarks={handleRemoveBookmarks}
          />
        </Route>
        <Route path="/movie/:idMovie">
          <ViewMovieDetail />
        </Route>
      </Switch>
    </Router>
  );
};
